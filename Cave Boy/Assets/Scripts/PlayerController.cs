﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float jumpPower = 25f;
    public float speed = 250f;
    public float maxSpeed = 10f;

    public bool grounded;
    public bool canMove;

    public bool hasAntiGravityPower = false;
    public bool hasSuperJumpPower = false;

    private Rigidbody2D rb;
    public float fallMultiplier = 8f;
    public float lowJumpMultiplier = 3f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        canMove = true;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump") && canMove)
        {
            if(grounded)
            {
                print("Jump");
                if (rb.gravityScale > 0)
                {
                    rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
                }
                else
                {
                    rb.AddForce(Vector2.down * jumpPower, ForceMode2D.Impulse);
                }
                
            }             
        }

        // Less floaty jumping: https://www.youtube.com/watch?v=7KiK0Aqtmzc
        if (rb.gravityScale > 0)
        {
            if (rb.velocity.y < 0)
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }
            else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
            }
        }
        else if(rb.gravityScale < 0)
        {
            if (rb.velocity.y > 0)
            {
                rb.velocity += Vector2.up * -Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }
            else if (rb.velocity.y < 0 && !Input.GetButton("Jump"))
            {
                rb.velocity += Vector2.up * -Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
            }
        }
    }

    private void FixedUpdate()
    {
        //Movement: https://www.youtube.com/watch?v=z8r5_mt3Wmw
        if (canMove)
        {
            float horizontal = Input.GetAxis("Horizontal");

            rb.AddForce((Vector2.right * speed) * horizontal);

            if (rb.velocity.x > maxSpeed)
            {
                rb.velocity = new Vector2(maxSpeed, rb.velocity.y);
            }

            if (rb.velocity.x < -maxSpeed)
            {
                rb.velocity = new Vector2(-maxSpeed, rb.velocity.y);
            }
        }
    }
}
