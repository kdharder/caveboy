﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject player;
	
	// Update is called once per frame
	void LateUpdate () {
        if (player == null)
        {
            player = GameObject.Find("Player(Clone)");
        }
        else
        {
            Vector3 pos = player.transform.position;
            transform.position = new Vector3(pos.x, pos.y, -10);
        }
	}
}
