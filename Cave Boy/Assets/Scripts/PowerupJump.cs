﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PowerupJump : MonoBehaviour {

    public Slider slider;

    public static float timeOfEffect = 10f;
    float oldJumpPower = 25;
    float newJumpPower = 40;

    PlayerController player;

    private void Start()
    {
        slider = GameObject.Find("Jump Slider").GetComponent<Slider>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            player = other.GetComponent<PlayerController>();
            //oldJumpPower = player.jumpPower;
            //newJumpPower = oldJumpPower + 15;
            if (!player.hasSuperJumpPower)
            {
                player.StartCoroutine(TimeDelay());
                Destroy(gameObject);
            }
        }
    }

    private IEnumerator TimeDelay()
    {
        player.jumpPower = newJumpPower;
        player.hasSuperJumpPower = true;

        for(float time = timeOfEffect; time > -1; time--)
        {
            slider.value = time / timeOfEffect;
            yield return new WaitForSeconds(1);
        }
        player.jumpPower = oldJumpPower;
        player.hasSuperJumpPower = false;
    }
}
