﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exit : MonoBehaviour {

    MapGenerator mapGenerator;
    Image levelImage;
    Text levelText;
    Canvas canvas;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            mapGenerator = GameObject.Find("Map Generator").GetComponent<MapGenerator>();
            PlayerController player = GameObject.Find("Player(Clone)").GetComponent<PlayerController>();
            player.canMove = false;
            player.StartCoroutine(NextLevelDisplay());
        }
    }

    IEnumerator NextLevelDisplay()
    {
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        levelImage = GameObject.Find("LevelImage").GetComponent<Image>();
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelImage.enabled = true;
        levelText.enabled = true;
        canvas.enabled = true;
        yield return new WaitForSeconds(2f);
        levelImage.enabled = false;
        levelText.enabled = false;
        canvas.enabled = false;

        mapGenerator.NextLevel();
    }
}
