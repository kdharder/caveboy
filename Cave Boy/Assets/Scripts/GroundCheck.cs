﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour {

    private PlayerController player;

    private void Start()
    {
        if (player == null)
        {
            player = GameObject.Find("Player(Clone)").GetComponentInParent<PlayerController>();
        }
        //player = GetComponentInParent<PlayerController>();
    }

    private void LateUpdate()
    {
        if (player == null)
        {
            player = GameObject.Find("Player(Clone)").GetComponentInParent<PlayerController>();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player.grounded = true;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        player.grounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        player.grounded = false;
    }
}
