﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PowerupAntiGravity : MonoBehaviour {
    public Slider slider;

    public static float normalGravityScale = 3f;
    public static float antiGravityScale = -3f;
    public static float timeOfEffect = 5f;

    PlayerController player;

    private void Start()
    {
        slider = GameObject.Find("Gravity Slider").GetComponent<Slider>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        print("Anti");
        if(other.tag == "Player")
        {
            player = other.GetComponent<PlayerController>();
            if (!player.hasAntiGravityPower)
            {
                player.StartCoroutine(TimeDelay());
                Destroy(gameObject);
            }
        }
    }

    private IEnumerator TimeDelay()
    {
        Rigidbody2D rb2d = player.GetComponent<Rigidbody2D>();

        rb2d.gravityScale = antiGravityScale;
        player.transform.Rotate(0, 0, 180);
        player.hasAntiGravityPower = true;

        for (float time = timeOfEffect; time > -1; time--)
        {
            slider.value = time / timeOfEffect;
            yield return new WaitForSeconds(1);
        }
        rb2d.gravityScale = normalGravityScale;
        player.transform.Rotate(0, 0, 180);
        player.hasAntiGravityPower = false;

    }
}
